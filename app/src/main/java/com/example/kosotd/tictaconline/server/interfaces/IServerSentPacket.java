package com.example.kosotd.tictaconline.server.interfaces;

import com.example.kosotd.tictaconline.server.utils.Packet;

/**
 * Created by kosotd on 20.01.2017.
 */

public interface IServerSentPacket {
    void onServerSentPacket(Packet packet);
}
