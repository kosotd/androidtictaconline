package com.example.kosotd.tictaconline.views.utils;

import android.graphics.Canvas;
import android.graphics.Paint;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by kosotd on 20.01.2017.
 */

public class TicTacBoard {
    public TicTacBoard(int width, int height){
        this.width = width;
        this.height = height;
        init();
    }

    public int getWidth(){
        return width;
    }

    public int getHeight(){
        return height;
    }

    public void setWidth(int width){
        this.width = width;
        init();
    }

    public void setHeight(int height){
        this.height = height;
        init();
    }

    private void init(){
        int width = getWidth();
        int height = getHeight();
        Line l1, l2, l3, l4;
        float x1 = width / 3;
        float x2 = width - x1;
        float step = (height - width) / 2;
        l1 = new Line(x1, step, x1, height - step);
        l2 = new Line(x2, step, x2, height - step);
        l3 = new Line(0, step + x1, width, step + x1);
        l4 = new Line(0, height - step - x1, width, height - step - x1);
        lines = new ArrayList<>(Arrays.asList(l1, l2, l3, l4));
        points = new Point[3][3];
        float xO = 0;
        float yO = step;
        float dx = x1;
        cellRadius = dx / 2;
        for (int i = 0; i < 3; ++i)
            for (int j = 0; j < 3; ++j)
                points[i][j] = new Point(xO + (i+1) * dx - (dx/2), yO + (j+1) * dx - (dx/2));
    }

    public void draw(Canvas canvas, Paint paint, int[][] field){
        for (Line l : lines)
            l.draw(canvas, paint);

        for (int i = 0; i < 3; ++i)
            for (int j = 0; j < 3; ++j){
                if (field[i][j] == 1)
                    drawX(canvas, paint, points[i][j]);
                if (field[i][j] == -1)
                    drawO(canvas, paint, points[i][j]);
            }
    }

    private void drawX(Canvas canvas, Paint drawPaint, Point point) {
        Line l1, l2;
        float r = (cellRadius * 3) / 4;
        l1 = new Line(point.getX() - r, point.getY() - r, point.getX() + r, point.getY() + r);
        l2 = new Line(point.getX() - r, point.getY() + r, point.getX() + r, point.getY() - r);
        l1.draw(canvas, drawPaint);
        l2.draw(canvas, drawPaint);
    }

    private void drawO(Canvas canvas, Paint drawPaint, Point point) {
        float r = (cellRadius * 3) / 4;
        point.draw(canvas, drawPaint, r);
    }

    public boolean pointInCell(float x, float y, IntPoint res) {
        float r = (cellRadius * 3) / 4;
        for (int i = 0; i < 3; ++i)
            for (int j = 0; j < 3; ++j)
                if (Math.pow(points[i][j].getX() - x, 2) + Math.pow(points[i][j].getY() - y, 2) < r * r) {
                    res.setX(i);
                    res.setY(j);
                    return true;
                }
        return false;
    }

    private int width, height;
    private Point[][] points;
    List<Line> lines;
    float cellRadius;
}
