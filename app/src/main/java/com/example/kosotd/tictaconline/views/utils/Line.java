package com.example.kosotd.tictaconline.views.utils;

import android.graphics.Canvas;
import android.graphics.Paint;

/**
 * Created by kosotd on 20.01.2017.
 */

public class Line {
    Point p1, p2;
    public Line(Point p1, Point p2){
        this.p1 = p1;
        this.p2 = p2;
    }
    public Line(float x1, float y1, float x2, float y2){
        this(new Point(x1, y1), new Point(x2, y2));
    }
    public void draw(Canvas canvas, Paint paint){
        canvas.drawLine(p1.getX(), p1.getY(), p2.getX(), p2.getY(), paint);
    }
}
