package com.example.kosotd.tictaconline.activitys;

import android.content.pm.ActivityInfo;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Toast;

import com.example.kosotd.tictaconline.server.RemoteConnection;
import com.example.kosotd.tictaconline.server.Server;
import com.example.kosotd.tictaconline.server.interfaces.IRemoteDisconnected;
import com.example.kosotd.tictaconline.server.interfaces.IRemoteSentPacket;
import com.example.kosotd.tictaconline.server.utils.GameAlrearyStartedPacket;
import com.example.kosotd.tictaconline.views.BoardView;
import com.example.kosotd.tictaconline.server.utils.ClientToServerPacket;
import com.example.kosotd.tictaconline.server.utils.EndGamePacket;
import com.example.kosotd.tictaconline.server.utils.Packet;
import com.example.kosotd.tictaconline.R;
import com.example.kosotd.tictaconline.server.utils.ServerToClientPacket;
import com.example.kosotd.tictaconline.server.utils.StartGamePacket;
import com.example.kosotd.tictaconline.views.interfaces.ISelectCell;

import java.net.SocketAddress;
import java.util.UUID;

public class GameBoardActivity extends AppCompatActivity implements IRemoteSentPacket, ISelectCell, IRemoteDisconnected {

    public static final String SERVER_ENDPOINT_EXTREA = "SERVER_ENDPOINT";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        setContentView(R.layout.activity_game_board);
        boardView = (BoardView) findViewById(R.id.boardView);

        if (!connectToServer()){
            Toast.makeText(getApplicationContext(), "Error when connecting to the server", Toast.LENGTH_LONG).show();
            finish();
        }
    }

    private boolean connectToServer(){
        SocketAddress endPoint = (SocketAddress)getIntent().getSerializableExtra(SERVER_ENDPOINT_EXTREA);
        remoteConnection = RemoteConnection.connectToServer(endPoint, false);
        if (remoteConnection != null){
            boardView.addSelectCellListener(this);
            remoteConnection.addRemoteSentPacketListener(this);
            remoteConnection.addRemoteDisconnectedListener(this);
            remoteConnection.startAccept();
            return true;
        }
        return false;
    }

    private void beforeDestroy(){
        if (Server.getInstance().isOpened())
            Server.getInstance().stop();
        if (remoteConnection != null){
            boardView.removeSelectCellListener(this);
            remoteConnection.removeRemoteSentPacketListener(this);
            remoteConnection.removeRemoteDisconnectedListener(this);
            remoteConnection.disconnect();
        }
    }

    @Override
    protected void onDestroy() {
        beforeDestroy();
        super.onDestroy();
    }

    @Override
    public void onRemoteSentPacket(UUID uuid, Packet packet) {
        if (packet instanceof StartGamePacket){
            boardView.startGame();
            repaintBoard();
        } else if (packet instanceof ServerToClientPacket){
            ServerToClientPacket stcPacket = (ServerToClientPacket)packet;
            boardView.setField(stcPacket.getField());
            repaintBoard();
        } else if (packet instanceof EndGamePacket){
            EndGamePacket egPacket = (EndGamePacket)packet;
            if (egPacket.isDraw())
                boardView.endGame();
            else
                boardView.endGame(egPacket.isYouWin());
                repaintBoard();
        } else if (packet instanceof GameAlrearyStartedPacket){
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    Toast.makeText(getApplicationContext(), "Game already started", Toast.LENGTH_LONG).show();
                }
            });
            beforeDestroy();
            finish();
        }
    }

    private void repaintBoard(){
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                boardView.invalidate();
            }
        });
    }

    @Override
    public void onSelectCell(int x, int y) {
        ClientToServerPacket pkg = new ClientToServerPacket(y, x);
        remoteConnection.sendPacket(pkg);
    }

    @Override
    public void onRemoteDisconnected(UUID uuid) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Toast.makeText(getApplicationContext(), "Connection is lost", Toast.LENGTH_LONG).show();
            }
        });
        beforeDestroy();
        finish();
    }

    private RemoteConnection remoteConnection;
    private BoardView boardView;
}
