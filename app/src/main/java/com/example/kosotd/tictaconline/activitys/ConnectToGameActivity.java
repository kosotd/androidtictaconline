package com.example.kosotd.tictaconline.activitys;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.PersistableBundle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;
import com.example.kosotd.tictaconline.R;

import java.net.Inet4Address;
import java.net.InetSocketAddress;
import java.net.SocketAddress;
import java.net.UnknownHostException;

public class ConnectToGameActivity extends AppCompatActivity {

    public static final String PREFS_NAME = "TicTacPrefsFile";
    private static final String HOST_BUNDLE = "HOST";
    private static final String PORT_BUNDLE = "PORT";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_connect_to_game);

        loadSettings();

        if (savedInstanceState != null){
            EditText textHostAddress = (EditText)findViewById(R.id.hostAddress);
            textHostAddress.setText(savedInstanceState.getString(HOST_BUNDLE));
            EditText textPort = (EditText)findViewById(R.id.port);
            textPort.setText(savedInstanceState.getString(PORT_BUNDLE));
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState, PersistableBundle outPersistentState) {
        EditText textHostAddress = (EditText)findViewById(R.id.hostAddress);
        EditText textPort = (EditText)findViewById(R.id.port);
        outState.putString(HOST_BUNDLE, textHostAddress.getText().toString());
        outState.putString(PORT_BUNDLE, textPort.getText().toString());
        super.onSaveInstanceState(outState, outPersistentState);
    }

    private void loadSettings(){
        SharedPreferences settings = getSharedPreferences(PREFS_NAME, 0);
        String host = settings.getString("hostAddress", "192.168.0.1");
        String port = settings.getString("port", "8080");
        EditText textHostAddress = (EditText)findViewById(R.id.hostAddress);
        textHostAddress.setText(host);
        EditText textPort = (EditText)findViewById(R.id.port);
        textPort.setText(port);
    }

    private void storeSettings(){
        SharedPreferences settings = getSharedPreferences(PREFS_NAME, 0);
        SharedPreferences.Editor editor = settings.edit();
        EditText textHostAddress = (EditText)findViewById(R.id.hostAddress);
        EditText textPort = (EditText)findViewById(R.id.port);
        editor.putString("hostAddress", textHostAddress.getText().toString());
        editor.putString("port", textPort.getText().toString());
        editor.commit();
    }

    private Inet4Address getHost() throws UnknownHostException {
        EditText textHostAddress = (EditText)findViewById(R.id.hostAddress);
        return (Inet4Address)Inet4Address.getByName(textHostAddress.getText().toString());
    }

    private int getPort() throws NumberFormatException{
        EditText textPort = (EditText)findViewById(R.id.port);
        return Integer.parseInt(textPort.getText().toString());
    }

    public void btnConnectClick(View view) {
        try {
            SocketAddress endPoint = new InetSocketAddress(getHost(), getPort());
            Intent intent = new Intent(this, GameBoardActivity.class);
            intent.putExtra(GameBoardActivity.SERVER_ENDPOINT_EXTREA, endPoint);
            storeSettings();
            startActivity(intent);
        } catch (UnknownHostException e) {
            Toast.makeText(getApplicationContext(), "Uncorrected host", Toast.LENGTH_LONG).show();
        } catch (NumberFormatException e){
            Toast.makeText(getApplicationContext(), "Uncorrected port", Toast.LENGTH_LONG).show();;
        } catch (Exception e){
            Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_LONG).show();;
        }
    }
}
