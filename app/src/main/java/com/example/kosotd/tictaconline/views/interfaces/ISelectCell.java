package com.example.kosotd.tictaconline.views.interfaces;

/**
 * Created by kosotd on 20.01.2017.
 */

public interface ISelectCell {
    void onSelectCell(int x, int y);
}