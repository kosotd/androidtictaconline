package com.example.kosotd.tictaconline.views.utils;

import android.graphics.Canvas;
import android.graphics.Paint;

/**
 * Created by kosotd on 20.01.2017.
 */

public class IntPoint {
    private int x, y;
    public IntPoint(int x, int y){
        this.x = x;
        this.y = y;
    }
    public IntPoint(){
    }
    public int getX(){ return x; }
    public int getY(){ return y; }
    public void setX(int x){ this.x = x; }
    public void setY(int y){ this.y = y; }
}
