package com.example.kosotd.tictaconline.views;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.os.Bundle;
import android.os.Parcelable;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;

import com.example.kosotd.tictaconline.activitys.GameBoardActivity;
import com.example.kosotd.tictaconline.server.Server;
import com.example.kosotd.tictaconline.views.interfaces.ISelectCell;
import com.example.kosotd.tictaconline.views.utils.IntPoint;
import com.example.kosotd.tictaconline.views.utils.TicTacBoard;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by kosotd on 19.01.2017.
 */

public class BoardView extends View {

    private final String INITITAL_MSG = "Wait for other palyer...";

    public BoardView(Context context) {
        super(context);
        init();
    }

    public BoardView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public BoardView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    public void addSelectCellListener(ISelectCell listener){
        selectCellListeners.add(listener);
    }

    public void removeSelectCellListener(ISelectCell listener) {
        selectCellListeners.remove(listener);
    }

    public void doOnSelectCell(int x, int y){
        for (ISelectCell listener : selectCellListeners)
            listener.onSelectCell(x, y);
    }

    @Override
    public boolean onTouchEvent( MotionEvent event ) {
        if (gameStarted){
            IntPoint cell = new IntPoint();
            if (ticTacBoard.pointInCell(event.getX(), event.getY(), cell))
                doOnSelectCell(cell.getX(), cell.getY());
        }
        return super.onTouchEvent(event);
    }

    @Override
    public void onDraw(Canvas canvas){
        super.onDraw(canvas);
        if (ticTacBoard.getWidth() != getWidth()) ticTacBoard.setWidth(getWidth());
        if (ticTacBoard.getHeight() != getHeight()) ticTacBoard.setHeight(getHeight());
        if (!gameStarted) {
            paint.setStrokeWidth(2);
            drawMessage(canvas, paint);
            return;
        }
        paint.setStrokeWidth(5);
        ticTacBoard.draw(canvas, paint, field);
    }

    public void startGame() {
        gameStarted = true;
    }

    public void endGame(boolean youWin) {
        gameStarted = false;
        msg = "You lose";
        if (youWin)
            msg = "You win";
    }

    public void endGame() {
        gameStarted = false;
        msg = "Draw";
    }

    public void setField(int[][] field){
        this.field = field;
    }

    private void drawMessage(Canvas canvas, Paint paint){
        canvas.drawText(msg, canvas.getWidth() / 2, canvas.getHeight() / 2, paint);
        if (!msg.equals(INITITAL_MSG) || !Server.getInstance().isOpened()) return;
        String msg1 = "Server adderss: ";
        canvas.drawText(msg1, canvas.getWidth() / 2, canvas.getHeight() / 2 + 50, paint);
        msg1 = Server.getInstance().getEndPoint().toString().replace("/", "");
        canvas.drawText(msg1, canvas.getWidth() / 2, canvas.getHeight() / 2 + 100, paint);
    }

    private Paint createBoardPaint(){
        Paint paint = new Paint();
        paint.setTextAlign(Paint.Align.CENTER);
        paint.setTextSize(36);
        paint.setAntiAlias(true);
        paint.setStrokeWidth(5);
        paint.setStyle(Paint.Style.STROKE);
        paint.setStrokeJoin(Paint.Join.ROUND);
        paint.setStrokeCap(Paint.Cap.ROUND);
        return paint;
    }

    private void init(){
        ticTacBoard = new TicTacBoard(getWidth(), getHeight());
        paint = createBoardPaint();
        selectCellListeners = new ArrayList<>();
        gameStarted = false;
        field = new int[3][3];
        msg = INITITAL_MSG;
    }

    Paint paint;
    List<ISelectCell> selectCellListeners;
    TicTacBoard ticTacBoard;
    private boolean gameStarted;
    String msg;
    private int[][] field;
}
