package com.example.kosotd.tictaconline.server.utils;

/**
 * Created by kosotd on 19.01.2017.
 */

public class EndGamePacket extends Packet {
    boolean youWin;
    boolean draw = false;

    public EndGamePacket(boolean youWin){
        this.youWin = youWin;
    }

    public EndGamePacket(){
        draw = true;
    }

    public boolean isDraw(){
        return draw;
    }
    public boolean isYouWin(){
        return youWin;
    }
}
