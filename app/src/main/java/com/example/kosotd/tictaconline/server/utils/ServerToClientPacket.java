package com.example.kosotd.tictaconline.server.utils;

/**
 * Created by kosotd on 19.01.2017.
 */

public class ServerToClientPacket extends Packet {
    private int[][] field;
    public ServerToClientPacket(int[][] field){
        this.field = field;
    }
    public int[][] getField(){
        return field;
    }
    public void setField(int[][] field){
        this.field = field;
    }
}
