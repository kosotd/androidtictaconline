package com.example.kosotd.tictaconline.activitys;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import com.example.kosotd.tictaconline.R;
import com.example.kosotd.tictaconline.server.Server;

import java.io.IOException;
import java.net.SocketAddress;
import java.net.UnknownHostException;

public class MenuActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);
    }

    public void btnStartTextClick(View view){
        Intent intent = new Intent(this, ConnectToGameActivity.class);
        startActivity(intent);
    }

    public void btnCreateServerClick(View view){
        try {
            Server.getInstance().startNew();
            Intent intent = new Intent(this, GameBoardActivity.class);
            intent.putExtra(GameBoardActivity.SERVER_ENDPOINT_EXTREA, Server.getInstance().getEndPoint());
            startActivity(intent);
        } catch (IOException e) {
            Toast.makeText(getApplicationContext(), "Server could not created", Toast.LENGTH_LONG).show();
        } catch (Exception e) {
            Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_LONG).show();
            if (Server.getInstance().isOpened())
                Server.getInstance().stop();
        }
    }

    public void btnExitClick(View view){
        finish();
        System.exit(0);
    }
}
