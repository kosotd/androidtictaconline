package com.example.kosotd.tictaconline.server.interfaces;

/**
 * Created by kosotd on 20.01.2017.
 */
public interface ISupplier<T> {
    T get();
}
