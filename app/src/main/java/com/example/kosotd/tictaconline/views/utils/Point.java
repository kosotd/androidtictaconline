package com.example.kosotd.tictaconline.views.utils;

import android.graphics.Canvas;
import android.graphics.Paint;

/**
 * Created by kosotd on 20.01.2017.
 */

public class Point {
    private float x, y;
    public Point(float x, float y){
        this.x = x;
        this.y = y;
    }
    public float getX(){ return x; }
    public float getY(){ return y; }
    public void setX(float x){ this.x = x; }
    public void setY(float y){ this.y = y; }
    public void draw(Canvas canvas, Paint paint, float radius){
        canvas.drawCircle(x, y, radius, paint);
    }
}
