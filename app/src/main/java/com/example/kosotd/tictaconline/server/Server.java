package com.example.kosotd.tictaconline.server;

import android.util.Log;
import com.example.kosotd.tictaconline.game.GameLogic;
import com.example.kosotd.tictaconline.server.interfaces.IClientConnected;
import com.example.kosotd.tictaconline.server.interfaces.IRemoteDisconnected;
import com.example.kosotd.tictaconline.server.interfaces.IRemoteSentPacket;
import com.example.kosotd.tictaconline.server.utils.Packet;
import java.io.IOException;
import java.net.Inet4Address;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.NetworkInterface;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketAddress;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;
import java.util.UUID;

/**
 * Created by kosotd on 19.01.2017.
 */

public class Server implements IRemoteSentPacket, IRemoteDisconnected {

    public static Server getInstance() {
        return ourInstance;
    }

    private Server() {
    }

    public boolean isOpened(){
        return isSererOpened;
    }

    private void disconnectAll() {
        for (RemoteConnection client : clients)
            client.disconnect();
        clients.clear();
    }

    private void startAccept() {
        isAcceptClients = true;
        final Server thisRef = this;
        acceptThread = new Thread(new Runnable() {
            @Override
            public void run() {
            while (isAcceptClients) {
                try {
                    Socket clientSocket = acceptConnectionsListener.accept();
                    if (!isAcceptClients) return;
                    final RemoteConnection client = new RemoteConnection(UUID.randomUUID(), clientSocket);
                    client.addRemoteSentPacketListener(thisRef);
                    client.addRemoteDisconnectedListener(thisRef);
                    clients.add(client);
                    new Thread(new Runnable() {
                        @Override
                        public void run() {
                            doClientConnected(client.getUuid());
                        }
                    }).start();
                } catch (Exception e) {
                }
            }
            }
        });
        acceptThread.start();
    }

    private void stopAccept() {
        isAcceptClients = false;
        if (acceptThread == null) return;
        try {
            Socket s = new Socket();
            s.connect(acceptConnectionsListener.getLocalSocketAddress());
            acceptThread.join();
            acceptThread = null;
        } catch (Exception e) {
            Log.d("", e.getMessage());
        }
    }

    private void doClientConnected(UUID uuid){
        for (IClientConnected listener : clientConnectedListeners)
            listener.onClientConnected(uuid);
    }

    private void doClientSentPacket(UUID uuid, Packet packet){
        for (IRemoteSentPacket listener : clientSentPacketListeners)
            listener.onRemoteSentPacket(uuid, packet);
    }

    private void doOnRemoteDisconnected(UUID uuid) {
        for (IRemoteDisconnected listener : remoteDisconnectedListeners)
            listener.onRemoteDisconnected(uuid);
    }

    @Override
    public void onRemoteDisconnected(UUID uuid) {
        doOnRemoteDisconnected(uuid);
    }

    @Override
    public void onRemoteSentPacket(UUID uuid, Packet packet) {
        doClientSentPacket(uuid, packet);
    }

    public void addClientConnectedListener(IClientConnected listener){
        clientConnectedListeners.add(listener);
    }

    public void removeClientConnectedListener(IClientConnected listener){
        clientConnectedListeners.remove(listener);
    }

    public void addClientSentPacketListener(IRemoteSentPacket listener){
        clientSentPacketListeners.add(listener);
    }

    public void removeClientSentPacketListener(IRemoteSentPacket listener){
        clientSentPacketListeners.remove(listener);
    }

    public void addRemoteDisconnectedListener(IRemoteDisconnected listener){
        remoteDisconnectedListeners.add(listener);
    }

    public void removeRemoteDisconnectedListener(IRemoteDisconnected listener){
        remoteDisconnectedListeners.remove(listener);
    }

    public void startNew() throws IOException {
        GameLogic.getInstance().init();
        if (isOpened())
            stop();
        isSererOpened = true;
        acceptConnectionsListener = new ServerSocket(port);
        startAccept();
    }

    public void stop(){
        Thread t = new Thread(new Runnable() {
            @Override
            public void run() {
                stopAccept();
                disconnectAll();
                close();
            }
        });
        t.start();
        try {
            t.join();
        } catch (Exception e) {
        }
        ourInstance = new Server();
    }

    private void close(){
        try {
            if (acceptConnectionsListener != null)
                acceptConnectionsListener.close();
        } catch (Exception e) {
        }
    }

    private InetAddress getIpAddress() {
        try {
            Enumeration<NetworkInterface> enumNetworkInterfaces = NetworkInterface.getNetworkInterfaces();
            while (enumNetworkInterfaces.hasMoreElements()) {
                NetworkInterface networkInterface = enumNetworkInterfaces.nextElement();
                Enumeration<InetAddress> enumInetAddress = networkInterface.getInetAddresses();
                while (enumInetAddress.hasMoreElements()) {
                    InetAddress inetAddress = enumInetAddress.nextElement();
                    if (inetAddress.isSiteLocalAddress())
                        return inetAddress;
                }
            }
        } catch (SocketException e) {
        }
        return null;
    }

    private RemoteConnection getClientByUuid(UUID uuid){
        for (RemoteConnection client : clients)
            if (client.toString().equals(uuid.toString()))
                return client;
        return null;
    }

    public void sendPacket(UUID uuid, Packet packet){
        RemoteConnection client = getClientByUuid(uuid);
        if (client == null) return;
        client.sendPacket(packet);
    }

    public void sendPacketToAll(Packet packet){
        for (RemoteConnection client : clients)
            client.sendPacket(packet);
    }

    public SocketAddress getEndPoint() {
        InetAddress inetAddress = getIpAddress();
        if (inetAddress == null)
            return acceptConnectionsListener.getLocalSocketAddress();
        return new InetSocketAddress(inetAddress, port);
    }

    private ServerSocket acceptConnectionsListener;
    private boolean isSererOpened = false;
    private int port = 8080;
    private boolean isAcceptClients = false;
    private static Server ourInstance = new Server();
    private List<IRemoteDisconnected> remoteDisconnectedListeners = new ArrayList<>();
    private List<IClientConnected> clientConnectedListeners = new ArrayList<>();
    private List<IRemoteSentPacket> clientSentPacketListeners = new ArrayList<>();
    private List<RemoteConnection> clients = new ArrayList<>();
    Thread acceptThread;

}
