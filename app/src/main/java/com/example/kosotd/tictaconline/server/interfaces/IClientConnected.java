package com.example.kosotd.tictaconline.server.interfaces;

import java.util.UUID;

/**
 * Created by kosotd on 20.01.2017.
 */

public interface IClientConnected{
    void onClientConnected(UUID uuid);
}