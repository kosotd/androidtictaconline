package com.example.kosotd.tictaconline.game;

import com.example.kosotd.tictaconline.server.interfaces.IClientConnected;
import com.example.kosotd.tictaconline.server.interfaces.IRemoteDisconnected;
import com.example.kosotd.tictaconline.server.interfaces.IRemoteSentPacket;
import com.example.kosotd.tictaconline.server.Server;
import com.example.kosotd.tictaconline.server.utils.ClientToServerPacket;
import com.example.kosotd.tictaconline.server.utils.EndGamePacket;
import com.example.kosotd.tictaconline.server.utils.GameAlrearyStartedPacket;
import com.example.kosotd.tictaconline.server.utils.Packet;
import com.example.kosotd.tictaconline.server.utils.ServerToClientPacket;
import com.example.kosotd.tictaconline.server.utils.StartGamePacket;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

/**
 * Created by kosotd on 19.01.2017.
 */

public class GameLogic implements IClientConnected, IRemoteSentPacket, IRemoteDisconnected {

    private static GameLogic ourInstance = new GameLogic();

    public static GameLogic getInstance() {
        return  ourInstance;
    }

    private GameLogic() {
    }

    public void init(){
        Server.getInstance().addClientConnectedListener(this);
        Server.getInstance().addClientSentPacketListener(this);
        Server.getInstance().addRemoteDisconnectedListener(this);
        first = null;
        second = null;
        field = new int[3][3];
        currMove = 1;
        gamers.clear();
    }

    @Override
    public void onClientConnected(UUID uuid) {
        if (first == null) {
            first = new Gamer(uuid, 1);
            gamers.add(first);
        }
        else if (second == null) {
            second = new Gamer(uuid, -1);
            gamers.add(second);
            startGame();
        } else {
            Server.getInstance().sendPacket(uuid, new GameAlrearyStartedPacket());
        }
    }

    @Override
    public void onRemoteDisconnected(UUID uuid) {
        Gamer gamer = getGamerByUuid(uuid);
        if (gamer != null){
            Gamer other = gamer.toString().equals(first.toString()) ? second : first;
            Server.getInstance().sendPacket(other.getUUID(), new EndGamePacket(true));
        }
    }

    private void startGame() {
        Server.getInstance().sendPacketToAll(new StartGamePacket());
    }

    @Override
    public void onRemoteSentPacket(UUID uuid, Packet packet) {
        if (!(packet instanceof ClientToServerPacket)) return;
        ClientToServerPacket ctsPacket = (ClientToServerPacket)packet;
        move(getGamerByUuid(uuid), ctsPacket.getCol(), ctsPacket.getRow());
        Gamer winner = getWinner();
        if (winner != null){
            if (three.toString().equals(winner.toString())){
                for (Gamer gamer : gamers)
                    Server.getInstance().sendPacket(gamer.getUUID(), new EndGamePacket());
            } else {
                Server.getInstance().sendPacket(winner.getUUID(), new EndGamePacket(true));
                Gamer other = winner.toString().equals(first.toString()) ? second : first;
                Server.getInstance().sendPacket(other.getUUID(), new EndGamePacket(false));
            }
            Server.getInstance().removeClientSentPacketListener(this);
        }
    }

    private Gamer getWinner(){
        int winner = GetGamerWinSign();
        if (winner == 1)
            return first;
        if (winner == -1)
            return second;
        if (winner == 2)
            return three;
        return null;
    }

    int GetGamerWinSign(){
        int dSum = 0;
        int d1Sum = 0;
        int fillCount = 0;
        for (int i = 0; i < 3; ++i){
            int hSum = 0;
            int vSum = 0;
            for (int j = 0; j < 3; ++j){
                hSum += field[i][j];
                if (i == j)
                    dSum += field[i][j];
                vSum += field[j][i];
                if (Math.abs(i + j) == 2)
                    d1Sum += field[j][i];
                if (field[i][j] != 0)
                    fillCount++;
            }
            if ((Math.abs(hSum) == 3) || (Math.abs(vSum) == 3))
                return Math.abs(vSum) > Math.abs(hSum) ? (int)Math.signum(vSum) : (int)Math.signum(hSum);
        }
        if ((Math.abs(dSum) == 3) || (Math.abs(d1Sum) == 3))
            return Math.abs(dSum) > Math.abs(d1Sum) ? (int)Math.signum(dSum) : (int)Math.signum(d1Sum);
        if (fillCount == 9) return 2;
        return 0;
    }

    private void move(Gamer gamer, int col, int row){
        if (currMove == gamer.getMove()){
            if (field[row][col] == 0) {
                field[row][col] = gamer.getMove();
                ServerToClientPacket pkg = new ServerToClientPacket(field);
                Server.getInstance().sendPacketToAll(pkg);
                currMove = -currMove;
            }
        }
    }

    private Gamer getGamerByUuid(UUID uuid){
        for (Gamer gamer : gamers)
            if (gamer.toString().equals(uuid.toString()))
                return gamer;
        return null;
    }

    int[][] field;
    Gamer first, second;
    int currMove;
    List<Gamer> gamers = new ArrayList<>();
    Gamer three = new Gamer(UUID.randomUUID(), 2);
}
