package com.example.kosotd.tictaconline.server.interfaces;

import com.example.kosotd.tictaconline.server.utils.Packet;

import java.util.UUID;

/**
 * Created by kosotd on 19.01.2017.
 */

public interface IRemoteSentPacket {
    void onRemoteSentPacket(UUID uuid, Packet packet);
}
