package com.example.kosotd.tictaconline.server.utils;

/**
 * Created by kosotd on 19.01.2017.
 */

public class ClientToServerPacket extends Packet {
    private int col, row;
    public int getCol(){
        return col;
    }

    public int getRow(){
        return row;
    }

    public ClientToServerPacket(int col, int row){
        this.col = col;
        this.row = row;
    }
}
