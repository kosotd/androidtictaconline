package com.example.kosotd.tictaconline.game;

import java.util.UUID;

/**
 * Created by kosotd on 20.01.2017.
 */

public class Gamer{

    public Gamer(UUID uuid, int move){
        this.uuid = uuid;
        this.move = move;
    }

    public UUID getUUID(){
        return uuid;
    }

    public int getMove() {
        return move;
    }

    public void setMove(int move){
        this.move = move;
    }

    @Override
    public String toString(){
        return uuid.toString();
    }

    private int move;
    private UUID uuid;
}
