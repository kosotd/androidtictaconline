package com.example.kosotd.tictaconline.server;

import android.support.annotation.NonNull;

import com.example.kosotd.tictaconline.server.interfaces.IRemoteDisconnected;
import com.example.kosotd.tictaconline.server.interfaces.IRemoteSentPacket;
import com.example.kosotd.tictaconline.server.utils.Packet;
import com.example.kosotd.tictaconline.server.utils.PingPacket;
import com.example.kosotd.tictaconline.server.utils.PongPacket;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.net.SocketAddress;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class RemoteConnection {

    public void addRemoteSentPacketListener(IRemoteSentPacket listener){
        remoteSentPacketListeners.add(listener);
    }

    public void removeRemoteSentPacketListener(IRemoteSentPacket listener){
        remoteSentPacketListeners.remove(listener);
    }

    public void addRemoteDisconnectedListener(IRemoteDisconnected listener){
        remoteDisconnectedListeners.add(listener);
    }

    public void removeRemoteDisconnectedListener(IRemoteDisconnected listener){
        remoteDisconnectedListeners.remove(listener);
    }

    private void doRemoteDisconnected(UUID uuid){
        for (IRemoteDisconnected listener : remoteDisconnectedListeners)
            listener.onRemoteDisconnected(uuid);
    }

    private void doRemoteSentPacket(Packet packet){
        for (IRemoteSentPacket listener : remoteSentPacketListeners)
            listener.onRemoteSentPacket(uuid, packet);
    }

    public RemoteConnection(UUID uuid, final @NonNull Socket socket) {
        this(uuid, socket, true);
    }

    public RemoteConnection(UUID uuid, final @NonNull Socket socket, boolean startAcceptOnCreate) {
        this.uuid = uuid;
        this.socket = socket;
        setPingThread();
        if (startAcceptOnCreate)
            startAccept();
    }

    public static RemoteConnection connectToServer(final SocketAddress endPoint, boolean startAcceptOnCreate){
        final Socket server = new Socket();
        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
            try {
                server.connect(endPoint);
            } catch (IOException e) {
            }
            }
        });
        thread.start();
        try {
            thread.join(10000);
        } catch (InterruptedException e) {
        }
        if (server.isConnected())
            return new RemoteConnection(UUID.randomUUID(), server, startAcceptOnCreate);
        return null;
    }

    public void stopAccept() {
        isAcceptData = false;
        if (acceptThread == null) return;
        try {
            acceptThread.join();
            acceptThread = null;
        } catch (Exception e) {
        }
    }

    public void startAccept() {
        isAcceptData = true;
        acceptThread = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    InputStream is = socket.getInputStream();
                    while (isAcceptData) {
                        while (is.available() > 0) {
                            ObjectInputStream socketInputStream = new ObjectInputStream(socket.getInputStream());
                            final Packet packet = (Packet) socketInputStream.readObject();
                            if (packet instanceof PingPacket)
                                sendPacket(new PongPacket());
                            else if (packet instanceof PongPacket)
                                pongReturned();
                            else
                                new Thread(new Runnable() {
                                    @Override
                                    public void run() {
                                        doRemoteSentPacket(packet);
                                    }
                                }).start();

                        }
                        if (getPing() > 10000)
                            new Thread(new Runnable() {
                                @Override
                                public void run() {
                                    doRemoteDisconnected(getUuid());
                                    disconnect();
                                }
                            }).start();
                    }
                } catch (Exception e) {
                }
            }
        });
        acceptThread.start();
    }

    private void setPingThread(){
        ping.pongReturned = true;
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    long pingDx = System.currentTimeMillis();
                    while (socket.isConnected()) {
                        if (System.currentTimeMillis() - pingDx > ping.ping)
                            ping.ping = System.currentTimeMillis() - pingDx;
                        if (ping.pongReturned) {
                            ping.ping = System.currentTimeMillis() - pingDx;
                            ping.pongReturned = false;
                            Thread.sleep(1000);
                            pingDx = System.currentTimeMillis();
                            sendPacket(new PingPacket());
                        }
                    }
                } catch (Exception e){
                }
            }
        }).start();
    }

    private void pongReturned() {
        ping.pongReturned = true;
    }

    public UUID getUuid() {
        return uuid;
    }

    public void disconnect() {
        stopAccept();
        try {
            socket.close();
        } catch (Exception e) {
        }
    }

    public void sendPacket(Packet packet) {
        try {
            synchronized (socket) {
                ObjectOutputStream socketOutputStream = new ObjectOutputStream(socket.getOutputStream());
                socketOutputStream.writeObject(packet);
                socketOutputStream.flush();
            }
        } catch (Exception e) {
        }
    }

    public long getPing(){
        return ping.ping;
    }

    @Override
    public String toString() {
        return uuid.toString();
    }

    private class Ping{
        public long ping;
        public boolean pongReturned;
    }

    private boolean isAcceptData;
    Thread acceptThread;
    private UUID uuid;
    private Ping ping = new Ping();
    private Socket socket;
    private ArrayList<IRemoteDisconnected> remoteDisconnectedListeners = new ArrayList<>();
    private List<IRemoteSentPacket> remoteSentPacketListeners = new ArrayList<>();
}
